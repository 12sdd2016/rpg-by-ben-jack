﻿Public Class Form3
    Dim filepath As String
    Dim arrClass(2) As classes
    Public saveFileLoaded As Integer
    Public arrSaveFiles(2) As saveFiles
    Public arrPutBack(2)
    Public playerPhys
    Public playerMgc
    Public playerHealth
    Public selectedClass As String
    Public selectedGender As String
    Public movingCharacterName As String

    Structure classes
        Dim id As Integer
        Dim className As String
        Dim str As Integer
        Dim mgc As Integer
        Dim hlt As Integer
        Dim special As String
    End Structure
    Structure saveFiles
        Dim name As String
        Dim className As Integer
        Dim gender As Integer
        Dim level As Integer
        Dim levelMap As Integer
        Dim excess As String
    End Structure
    Private Sub Form3_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        filepath = Application.StartupPath & "\"
        loadfile()
        loadSaveFiles()
        Form1.mapLevel = 1
        Form1.NewMap()

    End Sub
    Private Sub loadfile()
        'This loads in any external text files

        'Format
        ' ID | Class | attack | magic | health
        Dim fileReader As New System.IO.StreamReader(filepath & "classvalues.txt")
        Dim mineText As String = ""
        Dim bp
        Dim h As Integer

        While fileReader.Peek() <> -1
            mineText = fileReader.ReadLine()
            bp = Split(mineText, "|")
            arrClass(h).id = Int(bp(0))
            arrClass(h).className = bp(1)
            arrClass(h).str = Int(bp(2))
            arrClass(h).mgc = Int(bp(3))
            arrClass(h).hlt = Int(bp(4))
            arrClass(h).special = bp(5)
            h += 1
        End While

        
    End Sub
    Public Sub loadSaveFiles()
        Dim saveFileReader As New System.IO.StreamReader(filepath & "savefiles.txt")
        Dim lineOfText As String = ""
        Dim hp
        Dim y As Integer
        While saveFileReader.Peek() <> -1
            lineOfText = saveFileReader.ReadLine()
            hp = Split(lineOfText, "|")
            arrPutBack(y) = lineOfText
            arrSaveFiles(y).name = hp(0)
            arrSaveFiles(y).className = hp(1)
            arrSaveFiles(y).gender = hp(2)
            arrSaveFiles(y).level = hp(3)
            arrSaveFiles(y).levelMap = Int(hp(4))
            arrSaveFiles(y).excess = Int(hp(5))
            y += 1
        End While
        saveFileReader.Close()

        loadButtonOne.Text = arrSaveFiles(0).name
        loadButtonTwo.Text = arrSaveFiles(1).name
        loadButtonThree.Text = arrSaveFiles(2).name
        
    End Sub
    Private Sub classSelection_SelectedIndexChanged(sender As Object, e As EventArgs) Handles classSelection.SelectedIndexChanged
        Dim headTime As String = arrClass(classSelection.SelectedIndex).className
        strength.Text = arrClass(classSelection.SelectedIndex).str
        magic.Text = arrClass(classSelection.SelectedIndex).mgc
        health.Text = arrClass(classSelection.SelectedIndex).hlt
        specialAbility.Text = arrClass(classSelection.SelectedIndex).special
        If classSelection.SelectedIndex = 0 Then
            body.BackgroundImage = My.Resources.warrior
        ElseIf classSelection.SelectedIndex = 1 Then
            body.BackgroundImage = My.Resources.mage
        ElseIf classSelection.SelectedIndex = 2 Then
            body.BackgroundImage = My.Resources.paladin
        End If
    End Sub

    Private Sub playerPic()
        playerPhys = strength.Text
        playerMgc = magic.Text
        playerHealth = health.Text
        If classSelection.SelectedIndex = 0 Then
            selectedClass = "warrior"
            classChoice.Visible = False
            MapLoad.Enabled = True
        ElseIf classSelection.SelectedIndex = 1 Then
            selectedClass = "mage"
            classChoice.Visible = False
            MapLoad.Enabled = True
        ElseIf classSelection.SelectedIndex = 2 Then
            selectedClass = "paladin"
            classChoice.Visible = False
            MapLoad.Enabled = True
        Else
            MsgBox("Please choose a class")
        End If
        If genderSelection.SelectedIndex = 0 Then
            selectedGender = "male"
            classChoice.Visible = False
            MapLoad.Enabled = True
        ElseIf genderSelection.SelectedIndex = 1 Then
            selectedGender = "female"
            classChoice.Visible = False
            MapLoad.Enabled = True
        Else
            MsgBox("Please choose a gender")
        End If
        If nameSelection.Text = "" Then
            MsgBox("Please Choose a Name")
        ElseIf nameSelection.Text = "None" Then
            MsgBox("That is an invalid name")
        Else
        classChoice.Visible = False
        MapLoad.Enabled = True
        End If
        movingCharacterName = selectedClass + selectedGender
    End Sub

    Private Sub classChoice_Click(sender As Object, e As EventArgs) Handles classChoice.Click
        playerPic()
    End Sub
    Private Sub genderSelection_SelectedIndexChanged(sender As Object, e As EventArgs) Handles genderSelection.SelectedIndexChanged
        If genderSelection.SelectedIndex = 0 Then
            head.BackgroundImage = My.Resources.male1
        ElseIf genderSelection.SelectedIndex = 1 Then
            head.BackgroundImage = My.Resources.female1
        End If
    End Sub

    Public Sub delete_map()
        Dim mapFile As String = filepath & "maptest.txt"
        If System.IO.File.Exists(mapFile) Then
            System.IO.File.Delete(mapFile)
        End If
    End Sub

    Private Sub StartBtn_Click(sender As Object, e As EventArgs) Handles StartBtn.Click
        playerPic()
        Form1.mapLevel = arrSaveFiles(saveFileLoaded).levelMap
        Form1.playerLevelFormOne.Text = arrSaveFiles(saveFileLoaded).level
        Me.Hide()
        Form1.Show()
    End Sub

    Private Sub MapLoad_Click(sender As Object, e As EventArgs) Handles MapLoad.Click
        Form1.LoadEverything()
        MapLoad.Visible = False
        StartBtn.Enabled = True
    End Sub

    Private Sub loadButtonClicks(sender As Object, e As EventArgs) Handles loadButtonOne.Click, loadButtonTwo.Click, loadButtonThree.Click
        If sender.text = "None" Then
            If sender.text = loadButtonOne.Text Then
                saveFileLoaded = 0
                arrSaveFiles(0).level = 1
                arrSaveFiles(0).levelMap = 1
            ElseIf sender.text = loadButtonTwo.Text Then
                saveFileLoaded = 1
                arrSaveFiles(1).level = 1
                arrSaveFiles(1).levelMap = 1
            ElseIf sender.text = loadButtonThree.Text Then
                saveFileLoaded = 2
                arrSaveFiles(2).level = 1
                arrSaveFiles(2).levelMap = 1
            End If
            classSelection.Enabled = True
            genderSelection.Enabled = True
            nameSelection.Enabled = True
            loadButtonOne.Enabled = True
            loadButtonTwo.Enabled = True
            loadButtonThree.Enabled = True
            sender.enabled = False
        Else
            If sender.text = loadButtonOne.Text Then
                saveFileLoaded = 0
            ElseIf sender.text = loadButtonTwo.Text Then
                saveFileLoaded = 1
            ElseIf sender.text = loadButtonThree.Text Then
                saveFileLoaded = 2
            End If
            classSelection.Enabled = False
            genderSelection.Enabled = False
            nameSelection.Enabled = False
            loadButtonOne.Enabled = True
            loadButtonTwo.Enabled = True
            loadButtonThree.Enabled = True
            sender.enabled = False
            classSelection.SelectedIndex = arrSaveFiles(saveFileLoaded).className
            genderSelection.SelectedIndex = arrSaveFiles(saveFileLoaded).gender
            nameSelection.Text = arrSaveFiles(saveFileLoaded).name
            Form1.mapLevel = arrSaveFiles(saveFileLoaded).levelMap
            playerHealth = arrSaveFiles(saveFileLoaded).excess
            health.Text = playerHealth
            classChoice.Enabled = False
            MapLoad.Enabled = True
        End If
        
    End Sub

    Private Sub deleteButton1_Click(sender As Object, e As EventArgs) Handles deleteButton1.Click, deleteButton2.Click, deleteButton3.Click
        If sender.Tag = "one" Then
            arrSaveFiles(0).name = "None"
            loadButtonOne.Text = "None"
        ElseIf sender.Tag = "two" Then
            arrSaveFiles(1).name = "None"
            loadButtonTwo.Text = "None"
        ElseIf sender.tag = "three" Then
            arrSaveFiles(2).name = "None"
            loadButtonThree.Text = "None"
        End If
    End Sub
End Class