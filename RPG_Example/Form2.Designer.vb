﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.diceRollTimer = New System.Windows.Forms.Timer(Me.components)
        Me.enemyDice = New System.Windows.Forms.Label()
        Me.playerDice = New System.Windows.Forms.Label()
        Me.pHealth = New System.Windows.Forms.Label()
        Me.pAttack = New System.Windows.Forms.Label()
        Me.eAttack = New System.Windows.Forms.Label()
        Me.eHealth = New System.Windows.Forms.Label()
        Me.physicalAttack = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.enemyName = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.pMagic = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.battleNarration = New System.Windows.Forms.Label()
        Me.magicAttack = New System.Windows.Forms.Button()
        Me.head = New System.Windows.Forms.PictureBox()
        Me.body = New System.Windows.Forms.PictureBox()
        Me.enemy = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.playerLevel = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.enemyLevel = New System.Windows.Forms.Label()
        CType(Me.head, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.body, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.enemy, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'diceRollTimer
        '
        '
        'enemyDice
        '
        Me.enemyDice.AutoSize = True
        Me.enemyDice.BackColor = System.Drawing.Color.White
        Me.enemyDice.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.enemyDice.ForeColor = System.Drawing.Color.Black
        Me.enemyDice.Location = New System.Drawing.Point(67, 376)
        Me.enemyDice.Name = "enemyDice"
        Me.enemyDice.Size = New System.Drawing.Size(23, 25)
        Me.enemyDice.TabIndex = 0
        Me.enemyDice.Text = "4"
        Me.enemyDice.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'playerDice
        '
        Me.playerDice.AutoSize = True
        Me.playerDice.BackColor = System.Drawing.Color.White
        Me.playerDice.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.playerDice.Location = New System.Drawing.Point(487, 62)
        Me.playerDice.Name = "playerDice"
        Me.playerDice.Size = New System.Drawing.Size(23, 25)
        Me.playerDice.TabIndex = 1
        Me.playerDice.Text = "3"
        '
        'pHealth
        '
        Me.pHealth.AutoSize = True
        Me.pHealth.Location = New System.Drawing.Point(222, 115)
        Me.pHealth.Name = "pHealth"
        Me.pHealth.Size = New System.Drawing.Size(19, 13)
        Me.pHealth.TabIndex = 2
        Me.pHealth.Text = "10"
        '
        'pAttack
        '
        Me.pAttack.AutoSize = True
        Me.pAttack.Location = New System.Drawing.Point(222, 58)
        Me.pAttack.Name = "pAttack"
        Me.pAttack.Size = New System.Drawing.Size(13, 13)
        Me.pAttack.TabIndex = 3
        Me.pAttack.Text = "2"
        '
        'eAttack
        '
        Me.eAttack.AutoSize = True
        Me.eAttack.Location = New System.Drawing.Point(257, 349)
        Me.eAttack.Name = "eAttack"
        Me.eAttack.Size = New System.Drawing.Size(13, 13)
        Me.eAttack.TabIndex = 5
        Me.eAttack.Text = "2"
        '
        'eHealth
        '
        Me.eHealth.AutoSize = True
        Me.eHealth.Location = New System.Drawing.Point(257, 376)
        Me.eHealth.Name = "eHealth"
        Me.eHealth.Size = New System.Drawing.Size(19, 13)
        Me.eHealth.TabIndex = 4
        Me.eHealth.Text = "10"
        '
        'physicalAttack
        '
        Me.physicalAttack.Location = New System.Drawing.Point(294, 45)
        Me.physicalAttack.Name = "physicalAttack"
        Me.physicalAttack.Size = New System.Drawing.Size(81, 38)
        Me.physicalAttack.TabIndex = 6
        Me.physicalAttack.Text = "Attack with Physical"
        Me.physicalAttack.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(128, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 29)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "You"
        '
        'enemyName
        '
        Me.enemyName.AutoSize = True
        Me.enemyName.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold)
        Me.enemyName.Location = New System.Drawing.Point(454, 234)
        Me.enemyName.Name = "enemyName"
        Me.enemyName.Size = New System.Drawing.Size(78, 25)
        Me.enemyName.TabIndex = 8
        Me.enemyName.Text = "Enemy"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(168, 87)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(36, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Magic"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(157, 58)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Strength"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(166, 115)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(38, 13)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Health"
        '
        'pMagic
        '
        Me.pMagic.AutoSize = True
        Me.pMagic.Location = New System.Drawing.Point(222, 87)
        Me.pMagic.Name = "pMagic"
        Me.pMagic.Size = New System.Drawing.Size(26, 13)
        Me.pMagic.TabIndex = 12
        Me.pMagic.Text = "You"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(203, 376)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(38, 13)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Health"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(194, 349)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 13)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Strength"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(453, 116)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(54, 13)
        Me.Label8.TabIndex = 15
        Me.Label8.Text = "Your Dice"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(33, 429)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(64, 13)
        Me.Label9.TabIndex = 16
        Me.Label9.Text = "Enemy Dice"
        '
        'battleNarration
        '
        Me.battleNarration.AutoSize = True
        Me.battleNarration.Location = New System.Drawing.Point(233, 183)
        Me.battleNarration.Name = "battleNarration"
        Me.battleNarration.Size = New System.Drawing.Size(59, 13)
        Me.battleNarration.TabIndex = 17
        Me.battleNarration.Text = "Battle Start"
        Me.battleNarration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'magicAttack
        '
        Me.magicAttack.Location = New System.Drawing.Point(294, 104)
        Me.magicAttack.Name = "magicAttack"
        Me.magicAttack.Size = New System.Drawing.Size(81, 35)
        Me.magicAttack.TabIndex = 18
        Me.magicAttack.Text = "Attack with Magic"
        Me.magicAttack.UseVisualStyleBackColor = True
        '
        'head
        '
        Me.head.BackColor = System.Drawing.SystemColors.Control
        Me.head.Location = New System.Drawing.Point(-75, -13)
        Me.head.Name = "head"
        Me.head.Size = New System.Drawing.Size(160, 105)
        Me.head.TabIndex = 20
        Me.head.TabStop = False
        '
        'body
        '
        Me.body.BackColor = System.Drawing.SystemColors.Control
        Me.body.Location = New System.Drawing.Point(-120, -23)
        Me.body.Name = "body"
        Me.body.Size = New System.Drawing.Size(250, 300)
        Me.body.TabIndex = 19
        Me.body.TabStop = False
        '
        'enemy
        '
        Me.enemy.BackgroundImage = Global.RPG_Example.My.Resources.Resources.enemy
        Me.enemy.Location = New System.Drawing.Point(358, 265)
        Me.enemy.Name = "enemy"
        Me.enemy.Size = New System.Drawing.Size(250, 250)
        Me.enemy.TabIndex = 21
        Me.enemy.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = Global.RPG_Example.My.Resources.Resources.dice
        Me.PictureBox1.Location = New System.Drawing.Point(36, 335)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(100, 91)
        Me.PictureBox1.TabIndex = 22
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackgroundImage = Global.RPG_Example.My.Resources.Resources.dice
        Me.PictureBox2.Location = New System.Drawing.Point(456, 22)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(100, 91)
        Me.PictureBox2.TabIndex = 23
        Me.PictureBox2.TabStop = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(355, 276)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(120, 13)
        Me.Label10.TabIndex = 24
        Me.Label10.Text = """I'm going to stab you !"""
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(193, 15)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(65, 13)
        Me.Label11.TabIndex = 25
        Me.Label11.Text = "Player Level"
        '
        'playerLevel
        '
        Me.playerLevel.AutoSize = True
        Me.playerLevel.Location = New System.Drawing.Point(263, 15)
        Me.playerLevel.Name = "playerLevel"
        Me.playerLevel.Size = New System.Drawing.Size(13, 13)
        Me.playerLevel.TabIndex = 26
        Me.playerLevel.Text = "1"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(380, 246)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(68, 13)
        Me.Label12.TabIndex = 27
        Me.Label12.Text = "Enemy Level"
        '
        'enemyLevel
        '
        Me.enemyLevel.AutoSize = True
        Me.enemyLevel.Location = New System.Drawing.Point(361, 246)
        Me.enemyLevel.Name = "enemyLevel"
        Me.enemyLevel.Size = New System.Drawing.Size(13, 13)
        Me.enemyLevel.TabIndex = 28
        Me.enemyLevel.Text = "1"
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(587, 480)
        Me.Controls.Add(Me.enemyLevel)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.playerLevel)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.magicAttack)
        Me.Controls.Add(Me.battleNarration)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.pMagic)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.enemyName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.physicalAttack)
        Me.Controls.Add(Me.eAttack)
        Me.Controls.Add(Me.eHealth)
        Me.Controls.Add(Me.pAttack)
        Me.Controls.Add(Me.pHealth)
        Me.Controls.Add(Me.playerDice)
        Me.Controls.Add(Me.enemyDice)
        Me.Controls.Add(Me.head)
        Me.Controls.Add(Me.body)
        Me.Controls.Add(Me.enemy)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.PictureBox2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form2"
        CType(Me.head, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.body, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.enemy, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents diceRollTimer As System.Windows.Forms.Timer
    Friend WithEvents enemyDice As System.Windows.Forms.Label
    Friend WithEvents playerDice As System.Windows.Forms.Label
    Friend WithEvents pHealth As System.Windows.Forms.Label
    Friend WithEvents pAttack As System.Windows.Forms.Label
    Friend WithEvents eAttack As System.Windows.Forms.Label
    Friend WithEvents eHealth As System.Windows.Forms.Label
    Friend WithEvents physicalAttack As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents enemyName As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents pMagic As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents battleNarration As System.Windows.Forms.Label
    Friend WithEvents magicAttack As System.Windows.Forms.Button
    Friend WithEvents head As System.Windows.Forms.PictureBox
    Friend WithEvents body As System.Windows.Forms.PictureBox
    Friend WithEvents enemy As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents playerLevel As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents enemyLevel As System.Windows.Forms.Label
End Class
