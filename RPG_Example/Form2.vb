﻿Public Class Form2
    Dim rollno As Integer
    Dim rollnum As Integer
    Dim counter As Integer
    Dim enemyNames() As String = ({"", "John", "Steve", "Bucko", "Fred", "Bob", "Kapushkakov"})
    Dim enemyAttack As Integer
    Dim enemyHealth As Integer
    Dim enemyDamage As Integer
    Dim rollOnce As Boolean
    Dim killValue As Integer
    Public playerLevelValue As Integer

    Private Function getRN(max As Integer)
        ' Random number generator
        Dim x As Integer
        x = Int(Rnd() * max) + 1
        Return x
    End Function

    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        counter = 0
        Dim namer = getRN(6)
        enemyName.Text = enemyNames(namer)
        playerLevelValue = Form1.playerLevelFormOne.Text
        playerLevel.Text = Form1.playerLevelFormOne.Text
        pHealth.Text = Form1.currentHealth.Text + (playerLevel.Text * 1)
        pAttack.Text = Form3.playerPhys + (playerLevel.Text * 1)
        pMagic.Text = Form3.playerMgc + (playerLevel.Text * 1)
        enemyHealth = (15 - namer) + (Form1.mapLevel * 1)
        enemyAttack = (8 - namer) + (Form1.mapLevel * 0.5)
        eHealth.Text = enemyHealth
        eAttack.Text = enemyAttack
        head.BackgroundImage = Form3.head.BackgroundImage
        body.BackgroundImage = Form3.body.BackgroundImage
        rollOnce = False
        enemyLevel.Text = Form1.maplevel
    End Sub

    Private Sub damageCounting()
        eHealth.Text -= Int(rollno + killValue)
        If eHealth.Text <= 0 Then
            eHealth.Text = 0
        End If
    End Sub
    Private Sub enemyAttacks()
        pHealth.Text -= enemyDamage
        If pHealth.Text <= 0 Then
            pHealth.Text = 0
        End If
    End Sub

    Private Sub thingsIsDead()
        If pHealth.Text = 0 Then
            Dim result = MsgBox("You have died, would you like to restart? Pressing 'no' will quit the program", MsgBoxStyle.YesNo)
            If result = MsgBoxResult.Yes Then
                Application.Restart()
            ElseIf result = MsgBoxResult.No Then
                End
            End If
        ElseIf eHealth.Text = 0 Then
            playerLevelValue += (enemyLevel.Text * getRN(5))
            playerLevel.Text = playerLevelValue
            Dim result = MsgBox("You have slain your enemy", MsgBoxStyle.OkOnly)
            If result = MsgBoxResult.Ok Then
                playerLevelValue = playerLevel.Text
                Form1.playerLevelFormOne.Text = playerLevelValue
                Form1.currentHealth.Text = pHealth.Text
                Form1.mapStuff(Form1.gridX, Form1.gridY).item = "none"
                Form1.mapgrid(Form1.gridX, Form1.gridY).image = Nothing
                Me.Close()
            End If
        End If
    End Sub

    Private Sub textUpdate()
        battleNarration.Text = ""
        Dim arrAttacks(2)
        arrAttacks(0) = "Battle Log"
        If pHealth.Text > 0 And eHealth.Text > 0 Then
            For i = 1 To 2
                If i = 1 Then
                    arrAttacks(i) = "You deal " & Int(rollno + killValue) & " damage to the enemy"
                ElseIf i = 2 Then
                    arrAttacks(i) = "The enemy deals " & enemyDamage.ToString & " damage to you"
                End If
            Next
        ElseIf eHealth.Text = 0 And pHealth.Text = 0 Then
            For i = 1 To 2
                If i = 1 Then
                    arrAttacks(i) = "Both of you have died"
                ElseIf i = 2 Then
                    arrAttacks(i) = ""
                End If
            Next
        ElseIf eHealth.Text = 0 Then
            For i = 1 To 2
                If i = 1 Then
                    arrAttacks(i) = "The enemy is dead"
                ElseIf i = 2 Then
                    arrAttacks(i) = "The enemy deals " & enemyDamage.ToString & " damage to you"
                End If
            Next
        ElseIf pHealth.Text = 0 Then
            For i = 1 To 2
                If i = 1 Then
                    arrAttacks(i) = "You deal " & Int(rollno + killValue) & " damage to the enemy"
                ElseIf i = 2 Then
                    arrAttacks(i) = "You are dead"
                End If
            Next
        End If
        For i = 0 To 2
            battleNarration.Text += vbNewLine & arrAttacks(i)
        Next

    End Sub

    Private Sub damageOccurence()
        reenableEVERYTHING()
        damageCounting()
        enemyAttacks()
        textUpdate()
        thingsIsDead()
    End Sub
    Private Sub specialAbilityEffect()
        enemyDamage = Int(rollnum + eAttack.Text)
        If Form3.selectedClass = "paladin" And rollnum < 4 Then
            enemyDamage = Int(eAttack.Text)
            damageOccurence()
        ElseIf Form3.selectedClass = "mage" Then
            If rollOnce = False Then
                Dim result = MsgBox("Would you like to roll again?", MsgBoxStyle.YesNo)
                If result = MsgBoxResult.Yes Then
                    rollOnce = True
                    diceRollTimer.Enabled = True
                Else
                    damageOccurence()
                End If
            Else
                rollOnce = False
                damageOccurence()
            End If
        ElseIf Form3.selectedClass = "warrior" Then
            If playerDice.Text = 3 Then
                playerDice.Text = 6
            End If
            damageOccurence()
        Else
            damageOccurence()
        End If

    End Sub

    Private Sub diceRollTimer_Tick(sender As Object, e As EventArgs) Handles diceRollTimer.Tick
        rollno = getRN(6)
        playerDice.Text = rollno
        rollnum = getRN(6)
        enemyDice.Text = rollnum
        counter += 1
        If counter = 10 Then
            diceRollTimer.Interval = 200
        ElseIf counter = 18 Then
            diceRollTimer.Interval = 300
        ElseIf counter = 25 Then
            diceRollTimer.Enabled = False
            diceRollTimer.Interval = 100
            specialAbilityEffect()
            counter = 0
        End If
    End Sub

    Private Sub reenableEVERYTHING()
        magicAttack.Enabled = True
        physicalAttack.Enabled = True
    End Sub

    Private Sub physicalAttack_Click(sender As Object, e As EventArgs) Handles physicalAttack.Click
        killValue = pAttack.Text
        magicAttack.Enabled = False
        physicalAttack.Enabled = False
        diceRollTimer.Enabled = True
    End Sub
    Private Sub magicAttack_Click(sender As Object, e As EventArgs) Handles magicAttack.Click
        killValue = pMagic.Text
        magicAttack.Enabled = False
        physicalAttack.Enabled = False
        diceRollTimer.Enabled = True
    End Sub
End Class