﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form3
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.classSelection = New System.Windows.Forms.ComboBox()
        Me.strength = New System.Windows.Forms.Label()
        Me.magic = New System.Windows.Forms.Label()
        Me.health = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.genderSelection = New System.Windows.Forms.ComboBox()
        Me.nameSelection = New System.Windows.Forms.TextBox()
        Me.head = New System.Windows.Forms.PictureBox()
        Me.body = New System.Windows.Forms.PictureBox()
        Me.StartBtn = New System.Windows.Forms.Button()
        Me.specialAbility = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.MapLoad = New System.Windows.Forms.Button()
        Me.classChoice = New System.Windows.Forms.Button()
        Me.loadButtonOne = New System.Windows.Forms.Button()
        Me.loadButtonTwo = New System.Windows.Forms.Button()
        Me.loadButtonThree = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.deleteButton1 = New System.Windows.Forms.Button()
        Me.deleteButton3 = New System.Windows.Forms.Button()
        Me.deleteButton2 = New System.Windows.Forms.Button()
        CType(Me.head, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.body, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'classSelection
        '
        Me.classSelection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.classSelection.Enabled = False
        Me.classSelection.FormattingEnabled = True
        Me.classSelection.Items.AddRange(New Object() {"Warrior", "Mage", "Paladin"})
        Me.classSelection.Location = New System.Drawing.Point(136, 107)
        Me.classSelection.Name = "classSelection"
        Me.classSelection.Size = New System.Drawing.Size(121, 21)
        Me.classSelection.TabIndex = 0
        '
        'strength
        '
        Me.strength.AutoSize = True
        Me.strength.Location = New System.Drawing.Point(233, 142)
        Me.strength.Name = "strength"
        Me.strength.Size = New System.Drawing.Size(10, 13)
        Me.strength.TabIndex = 1
        Me.strength.Text = " "
        '
        'magic
        '
        Me.magic.AutoSize = True
        Me.magic.Location = New System.Drawing.Point(233, 175)
        Me.magic.Name = "magic"
        Me.magic.Size = New System.Drawing.Size(10, 13)
        Me.magic.TabIndex = 2
        Me.magic.Text = " "
        '
        'health
        '
        Me.health.AutoSize = True
        Me.health.Location = New System.Drawing.Point(233, 209)
        Me.health.Name = "health"
        Me.health.Size = New System.Drawing.Size(10, 13)
        Me.health.TabIndex = 3
        Me.health.Text = " "
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(179, 209)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Health"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(181, 175)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(36, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Magic"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(171, 142)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Physical"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 50.0!)
        Me.Label4.Location = New System.Drawing.Point(50, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(486, 89)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "RPG Adventure"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(32, 110)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Choose a Class"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(22, 244)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(90, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Choose a Gender"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(9, 291)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(103, 13)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "What is your Name?"
        '
        'genderSelection
        '
        Me.genderSelection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.genderSelection.Enabled = False
        Me.genderSelection.FormattingEnabled = True
        Me.genderSelection.Items.AddRange(New Object() {"Male", "Female"})
        Me.genderSelection.Location = New System.Drawing.Point(136, 241)
        Me.genderSelection.Name = "genderSelection"
        Me.genderSelection.Size = New System.Drawing.Size(121, 21)
        Me.genderSelection.TabIndex = 12
        '
        'nameSelection
        '
        Me.nameSelection.Enabled = False
        Me.nameSelection.Location = New System.Drawing.Point(136, 288)
        Me.nameSelection.Name = "nameSelection"
        Me.nameSelection.Size = New System.Drawing.Size(121, 20)
        Me.nameSelection.TabIndex = 13
        '
        'head
        '
        Me.head.BackColor = System.Drawing.Color.White
        Me.head.Location = New System.Drawing.Point(348, 117)
        Me.head.Name = "head"
        Me.head.Size = New System.Drawing.Size(160, 105)
        Me.head.TabIndex = 15
        Me.head.TabStop = False
        '
        'body
        '
        Me.body.BackColor = System.Drawing.Color.White
        Me.body.Location = New System.Drawing.Point(303, 107)
        Me.body.Name = "body"
        Me.body.Size = New System.Drawing.Size(250, 300)
        Me.body.TabIndex = 14
        Me.body.TabStop = False
        '
        'StartBtn
        '
        Me.StartBtn.Enabled = False
        Me.StartBtn.Location = New System.Drawing.Point(65, 420)
        Me.StartBtn.Name = "StartBtn"
        Me.StartBtn.Size = New System.Drawing.Size(75, 23)
        Me.StartBtn.TabIndex = 16
        Me.StartBtn.Text = "Start Game"
        Me.StartBtn.UseVisualStyleBackColor = True
        '
        'specialAbility
        '
        Me.specialAbility.Location = New System.Drawing.Point(10, 162)
        Me.specialAbility.Name = "specialAbility"
        Me.specialAbility.Size = New System.Drawing.Size(149, 60)
        Me.specialAbility.TabIndex = 17
        Me.specialAbility.Text = " "
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(9, 142)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(72, 13)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Special Ability"
        '
        'MapLoad
        '
        Me.MapLoad.Enabled = False
        Me.MapLoad.Location = New System.Drawing.Point(157, 379)
        Me.MapLoad.Name = "MapLoad"
        Me.MapLoad.Size = New System.Drawing.Size(75, 23)
        Me.MapLoad.TabIndex = 17
        Me.MapLoad.Text = "Load Map"
        Me.MapLoad.UseVisualStyleBackColor = True
        '
        'classChoice
        '
        Me.classChoice.Location = New System.Drawing.Point(65, 379)
        Me.classChoice.Name = "classChoice"
        Me.classChoice.Size = New System.Drawing.Size(75, 23)
        Me.classChoice.TabIndex = 19
        Me.classChoice.Text = "Choose"
        Me.classChoice.UseVisualStyleBackColor = True
        '
        'loadButtonOne
        '
        Me.loadButtonOne.Location = New System.Drawing.Point(303, 434)
        Me.loadButtonOne.Name = "loadButtonOne"
        Me.loadButtonOne.Size = New System.Drawing.Size(75, 23)
        Me.loadButtonOne.TabIndex = 20
        Me.loadButtonOne.Text = "Button1"
        Me.loadButtonOne.UseVisualStyleBackColor = True
        '
        'loadButtonTwo
        '
        Me.loadButtonTwo.Location = New System.Drawing.Point(391, 434)
        Me.loadButtonTwo.Name = "loadButtonTwo"
        Me.loadButtonTwo.Size = New System.Drawing.Size(75, 23)
        Me.loadButtonTwo.TabIndex = 21
        Me.loadButtonTwo.Text = "Button2"
        Me.loadButtonTwo.UseVisualStyleBackColor = True
        '
        'loadButtonThree
        '
        Me.loadButtonThree.Location = New System.Drawing.Point(478, 434)
        Me.loadButtonThree.Name = "loadButtonThree"
        Me.loadButtonThree.Size = New System.Drawing.Size(75, 23)
        Me.loadButtonThree.TabIndex = 22
        Me.loadButtonThree.Text = "Button3"
        Me.loadButtonThree.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(386, 417)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(89, 13)
        Me.Label8.TabIndex = 23
        Me.Label8.Text = "Load a Character"
        '
        'deleteButton1
        '
        Me.deleteButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.deleteButton1.Location = New System.Drawing.Point(318, 457)
        Me.deleteButton1.Name = "deleteButton1"
        Me.deleteButton1.Size = New System.Drawing.Size(43, 19)
        Me.deleteButton1.TabIndex = 24
        Me.deleteButton1.Tag = "one"
        Me.deleteButton1.Text = "Clear"
        Me.deleteButton1.UseVisualStyleBackColor = True
        '
        'deleteButton3
        '
        Me.deleteButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.deleteButton3.Location = New System.Drawing.Point(493, 457)
        Me.deleteButton3.Name = "deleteButton3"
        Me.deleteButton3.Size = New System.Drawing.Size(43, 19)
        Me.deleteButton3.TabIndex = 25
        Me.deleteButton3.Tag = "three"
        Me.deleteButton3.Text = "Clear"
        Me.deleteButton3.UseVisualStyleBackColor = True
        '
        'deleteButton2
        '
        Me.deleteButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.deleteButton2.Location = New System.Drawing.Point(406, 457)
        Me.deleteButton2.Name = "deleteButton2"
        Me.deleteButton2.Size = New System.Drawing.Size(43, 19)
        Me.deleteButton2.TabIndex = 26
        Me.deleteButton2.Tag = "two"
        Me.deleteButton2.Text = "Clear"
        Me.deleteButton2.UseVisualStyleBackColor = True
        '
        'Form3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(587, 480)
        Me.Controls.Add(Me.deleteButton2)
        Me.Controls.Add(Me.deleteButton3)
        Me.Controls.Add(Me.deleteButton1)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.loadButtonThree)
        Me.Controls.Add(Me.loadButtonTwo)
        Me.Controls.Add(Me.loadButtonOne)
        Me.Controls.Add(Me.classChoice)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.specialAbility)
        Me.Controls.Add(Me.MapLoad)
        Me.Controls.Add(Me.StartBtn)
        Me.Controls.Add(Me.head)
        Me.Controls.Add(Me.body)
        Me.Controls.Add(Me.nameSelection)
        Me.Controls.Add(Me.genderSelection)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.health)
        Me.Controls.Add(Me.magic)
        Me.Controls.Add(Me.strength)
        Me.Controls.Add(Me.classSelection)
        Me.Name = "Form3"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form3"
        CType(Me.head, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.body, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents classSelection As System.Windows.Forms.ComboBox
    Friend WithEvents strength As System.Windows.Forms.Label
    Friend WithEvents magic As System.Windows.Forms.Label
    Friend WithEvents health As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents genderSelection As System.Windows.Forms.ComboBox
    Friend WithEvents nameSelection As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents body As System.Windows.Forms.PictureBox
    Friend WithEvents head As System.Windows.Forms.PictureBox
    Friend WithEvents StartBtn As System.Windows.Forms.Button


    Friend WithEvents specialAbility As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label

    Friend WithEvents MapLoad As System.Windows.Forms.Button
    Friend WithEvents classChoice As System.Windows.Forms.Button
    Friend WithEvents loadButtonOne As System.Windows.Forms.Button
    Friend WithEvents loadButtonTwo As System.Windows.Forms.Button
    Friend WithEvents loadButtonThree As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents deleteButton1 As System.Windows.Forms.Button
    Friend WithEvents deleteButton3 As System.Windows.Forms.Button
    Friend WithEvents deleteButton2 As System.Windows.Forms.Button


End Class
