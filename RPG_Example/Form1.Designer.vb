﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.infoStrip = New System.Windows.Forms.StatusStrip()
        Me.infoStripMapTile = New System.Windows.Forms.ToolStripStatusLabel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.mapLevelDisplay = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.currentHealth = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.playerLevelFormOne = New System.Windows.Forms.Label()
        Me.infoStrip.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'infoStrip
        '
        Me.infoStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.infoStripMapTile})
        Me.infoStrip.Location = New System.Drawing.Point(0, 458)
        Me.infoStrip.Name = "infoStrip"
        Me.infoStrip.Size = New System.Drawing.Size(587, 22)
        Me.infoStrip.TabIndex = 0
        Me.infoStrip.Text = "StatusStrip1"
        '
        'infoStripMapTile
        '
        Me.infoStripMapTile.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.infoStripMapTile.Name = "infoStripMapTile"
        Me.infoStripMapTile.Size = New System.Drawing.Size(0, 17)
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(491, -11)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(100, 501)
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(494, 60)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(90, 20)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Map Level"
        '
        'mapLevelDisplay
        '
        Me.mapLevelDisplay.AutoSize = True
        Me.mapLevelDisplay.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mapLevelDisplay.ForeColor = System.Drawing.Color.White
        Me.mapLevelDisplay.Location = New System.Drawing.Point(530, 87)
        Me.mapLevelDisplay.Name = "mapLevelDisplay"
        Me.mapLevelDisplay.Size = New System.Drawing.Size(13, 13)
        Me.mapLevelDisplay.TabIndex = 6
        Me.mapLevelDisplay.Text = "1"
        '
        'Label2
        '
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(496, 146)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 83)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Key Presses:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "f - Save" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "w - Up" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "a - Left" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "s - Down" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "d - Right" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'currentHealth
        '
        Me.currentHealth.AutoSize = True
        Me.currentHealth.ForeColor = System.Drawing.Color.White
        Me.currentHealth.Location = New System.Drawing.Point(527, 39)
        Me.currentHealth.Name = "currentHealth"
        Me.currentHealth.Size = New System.Drawing.Size(19, 13)
        Me.currentHealth.TabIndex = 8
        Me.currentHealth.Text = "20"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(502, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(74, 25)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Health"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(507, 109)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Your Level"
        '
        'playerLevelFormOne
        '
        Me.playerLevelFormOne.AutoSize = True
        Me.playerLevelFormOne.ForeColor = System.Drawing.Color.White
        Me.playerLevelFormOne.Location = New System.Drawing.Point(506, 130)
        Me.playerLevelFormOne.Name = "playerLevelFormOne"
        Me.playerLevelFormOne.Size = New System.Drawing.Size(13, 13)
        Me.playerLevelFormOne.TabIndex = 11
        Me.playerLevelFormOne.Text = "1"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(587, 480)
        Me.Controls.Add(Me.playerLevelFormOne)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.currentHealth)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.mapLevelDisplay)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.infoStrip)
        Me.Controls.Add(Me.PictureBox1)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form1"
        Me.infoStrip.ResumeLayout(False)
        Me.infoStrip.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents infoStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents infoStripMapTile As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents mapLevelDisplay As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents currentHealth As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents playerLevelFormOne As System.Windows.Forms.Label

End Class
