﻿Imports System.IO

Public Class Form1

    Public Const gridX As Integer = 8
    Public Const gridY As Integer = 8

    Dim PlayerItems() As String = {"some Insults", "a bagel", "a generic sword", "a football", "Code"}
    Dim filepath As String = Application.StartupPath & "\"
    Public mapgrid(gridX, gridY) As PictureBox
    Public mapStuff(gridX, gridY) As map
    Dim dudeX As Integer = 3
    Dim dudeY As Integer = 3
    Dim PortalX As Integer
    Dim PortalY As Integer
    Dim ChestX As Integer
    Dim ChestY As Integer
    Dim dudeColor As Color = Color.LawnGreen
    Dim THEPLAYER As PictureBox
    Public charLevel As Integer
    Public mapLevel As Integer

    Structure map
        Dim x As Integer
        Dim y As Integer
        Dim terrain As String
        Dim canpass As Boolean
        Dim item As String
    End Structure

    Private Sub Form1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress

        mapgrid(dudeX, dudeY).BackColor = getMapColor(mapStuff(dudeX, dudeY).terrain)

        Select Case e.KeyChar
            Case "f"
                'Save your character
                saveFiles()
            Case "p"
                Form2.Show()
            Case "o"
                Form3.Show()
            Case "w"
                If dudeY > 0 Then
                    If mapStuff(dudeX, dudeY - 1).canpass = True Then
                        dudeY -= 1
                        For x = 0 To gridX
                            For y = 0 To gridY
                                mapgrid(x, y).Top += 64
                            Next
                        Next
                        Intersection()
                        MonsterMovement()
                    End If
                End If
            Case "s"
                If dudeY < gridY Then
                    If mapStuff(dudeX, dudeY + 1).canpass = True Then
                        dudeY += 1
                        For x = 0 To gridX
                            For y = 0 To gridY
                                mapgrid(x, y).Top -= 64
                            Next
                        Next
                        Intersection()
                        MonsterMovement()
                    End If
                End If
            Case "a"
                If dudeX > 0 Then
                    If mapStuff(dudeX - 1, dudeY).canpass = True Then
                        dudeX -= 1
                        For x = 0 To gridX
                            For y = 0 To gridY
                                mapgrid(x, y).Left += 64
                            Next
                        Next
                        Intersection()
                        MonsterMovement()
                    End If
                End If
            Case "d"
                If dudeX < gridX Then
                    If mapStuff(dudeX + 1, dudeY).canpass = True Then
                        dudeX += 1
                        For x = 0 To gridX
                            For y = 0 To gridY
                                mapgrid(x, y).Left -= 64
                            Next
                        Next
                        Intersection()
                        MonsterMovement()
                    End If
                End If
        End Select

        'mapgrid(dudeX, dudeY).BackColor = dudeColor
        infoStripMapTile.Text = mapStuff(dudeX, dudeY).terrain

    End Sub

    Public Sub saveFiles()
        Dim arrSave(5)
        Dim filename = filepath & "saveFiles.txt"
        Dim saveLine As String = ""
        arrSave(0) = Form3.nameSelection.Text
        arrSave(1) = Form3.classSelection.SelectedIndex
        arrSave(2) = Form3.genderSelection.SelectedIndex
        arrSave(3) = playerLevelFormOne.Text
        arrSave(4) = Int(mapLevel)
        arrSave(5) = currentHealth.Text

        For i = 0 To 5
            saveLine += arrSave(i) & "|"
        Next

        Form3.arrPutBack(Form3.saveFileLoaded) = saveLine

        System.IO.File.Open("saveFiles.txt", System.IO.FileMode.Truncate).Close()
        Dim objWriter As New System.IO.StreamWriter(filename, True)
        For i = 0 To 2
            objWriter.WriteLine(Form3.arrPutBack(i))
        Next
        objWriter.Close()

    End Sub

    Public Sub LoadEverything()

        'Loads in everything needed (map, player, enemies, portal)
        'Initiates when player clicks "Load Map" button on the start screen
        createMap()
        load_map()
        draw_map()
        loadPortal()
        loadChest()
        System.Threading.Thread.Sleep(150)
        LoadMonsters()
        mapgrid(dudeX, dudeY).BackColor = dudeColor

    End Sub

    Public Sub FormLoad() Handles Me.Load
        mapLevelDisplay.Text = mapLevel
        currentHealth.Text = Form3.playerHealth
        'charLevel = Form3.arrSaveFiles(Form3.saveFileLoaded).level
        playerBox()
    End Sub

    Private Sub draw_map()

        For x = 0 To gridX
            For y = 0 To gridY
                mapgrid(x, y).BackColor = getMapColor(mapStuff(x, y).terrain)
            Next
        Next

    End Sub

    Private Function getMapColor(mapTerrain As String) As Color
        Select Case mapTerrain
            Case "lava"
                Return Color.Red
            Case "grass"
                Return Color.LawnGreen
            Case "water"
                Return Color.Aquamarine
            Case "mountain"
                Return Color.DarkSlateGray
            Case "forest"
                Return Color.DarkGreen
            Case "portal"
                Return Color.Purple
            Case "chest"
                Return Color.Yellow
        End Select
    End Function

    Private Sub createMap()

        For x = 0 To gridX
            For y = 0 To gridY
                Dim pic As New PictureBox
                pic.Left = 64 * x - 15
                pic.Top = 64 * y - 15
                pic.Height = 64
                pic.Width = 64
                mapgrid(x, y) = pic
                pic.BackColor = Color.Plum
                Me.Controls.Add(pic)
            Next
        Next
    End Sub

    Private Sub load_map()

        'Dim oFile As System.IO.File
        Dim objReader As New System.IO.StreamReader(filepath & "maptest.txt")
        Dim lineIn As String
        Dim tmp

        ' Read in each line from the file
        Do While objReader.Peek() <> -1     ' checks to see if the file has more lines 
            lineIn = objReader.ReadLine()   ' reads a line into a string
            tmp = Split(lineIn, "|")        ' splits the text into fields End While
            mapStuff(Int(tmp(0)), Int(tmp(1))).x = tmp(0)
            mapStuff(Int(tmp(0)), Int(tmp(1))).y = tmp(1)
            mapStuff(Int(tmp(0)), Int(tmp(1))).terrain = tmp(2)
            mapStuff(Int(tmp(0)), Int(tmp(1))).canpass = CBool(tmp(3))
            mapStuff(Int(tmp(0)), Int(tmp(1))).item = tmp(4)
        Loop

        objReader.Close()

    End Sub

    Private Sub playerBox()
        Dim player As New PictureBox
        player.Height = 50
        player.Width = 50
        player.Left = 184
        player.Top = 184
        player.BackColor = Color.LawnGreen
        Dim playerImage As String = Form3.movingCharacterName
        If playerImage = "warriormale" Then
            player.Image = My.Resources.warriormale
        ElseIf playerImage = "warriorfemale" Then
            player.Image = My.Resources.warriorfemale
        ElseIf playerImage = "magemale" Then
            player.Image = My.Resources.magemale
        ElseIf playerImage = "magefemale" Then
            player.Image = My.Resources.magefemale
        ElseIf playerImage = "paladinmale" Then
            player.Image = My.Resources.paladinmale
        ElseIf playerImage = "paladinfemale" Then
            player.Image = My.Resources.paladinfemale
        Else
            player.BackColor = Color.White
        End If
        THEPLAYER = player
        Me.Controls.Add(player)
        player.BringToFront()
    End Sub

    Public Sub NewMap()
        'Deletes the existing map to make way for the generating of new map file
        Form3.delete_map()
        Process.Start(filepath & "mapmaker.py")
    End Sub

    Private Sub loadPortal()
        Dim RN As Integer = RandomNumber(0, 10)                'Decides which tile the portal will be placed upon
        Dim CN As Integer = 0

        For X = 0 To gridX
            For Y = 0 To gridY
                If mapStuff(X, Y).terrain = "grass" Then    'Only places portal on grass so the player can walk on it
                    If RN = CN Then
                        mapStuff(X, Y).terrain = "portal"
                        mapgrid(X, Y).BackColor = Color.Purple
                        PortalX = X
                        PortalY = Y
                    End If
                    CN += 1
                End If
            Next
        Next
    End Sub

    Private Sub Intersection()

        'Portals - going to nepw map
        If THEPLAYER.Bounds.IntersectsWith(mapgrid(PortalX, PortalY).Bounds) Then
            NewMap()
            System.Threading.Thread.Sleep(300)
            load_map()
            For x = 0 To gridX
                For y = 0 To gridY
                    mapgrid(x, y).BackColor = getMapColor(mapStuff(x, y).terrain)
                    mapgrid(x, y).Image = Nothing
                Next
            Next

            mapLevel += 1
            mapLevelDisplay.Text = mapLevel

            LoadMonsters()
            loadPortal()
            loadChest()


        End If

        'Initiates combat upon coming into contact with monster
        'Also opens chest upon contact to find items. Hurray! + Colour changes on different tiles
        For x = 0 To gridX
            For y = 0 To gridY
                If mapStuff(x, y).item = "monster" Then
                    If THEPLAYER.Bounds.IntersectsWith(mapgrid(x, y).Bounds) Then
                        Form2.Show()
                    End If
                ElseIf mapStuff(x, y).terrain = "chest" Then
                    If THEPLAYER.Bounds.IntersectsWith(mapgrid(x, y).Bounds) Then
                        MsgBox("You have found a chest!!")
                        ItemDrop()
                        mapStuff(x, y).terrain = "grass"
                        mapgrid(x, y).BackColor = Color.LawnGreen
                    End If
                ElseIf mapStuff(x, y).terrain = "forest" Then
                    If THEPLAYER.Bounds.IntersectsWith(mapgrid(x, y).Bounds) Then
                        THEPLAYER.BackColor = Color.DarkGreen
                    End If
                ElseIf mapStuff(x, y).terrain = "grass" Then
                    If THEPLAYER.Bounds.IntersectsWith(mapgrid(x, y).Bounds) Then
                        THEPLAYER.BackColor = Color.LawnGreen
                    End If
                End If
            Next
        Next

    End Sub

    Private Sub LoadMonsters()
        'Loads in monsters on the monster tiles that are defined in the map file
        For x = 0 To gridX
            For y = 0 To gridY
                If mapStuff(x, y).item = "monster" Then
                    If mapStuff(x, y).terrain = "grass" Then
                        mapgrid(x, y).Image = My.Resources.smallEnemy
                    Else
                        mapStuff(x, y).item = "none"
                    End If
                End If
            Next
        Next
    End Sub

    Private Sub loadChest()
        Dim RN As Integer = RandomNumber(10, 15)                'Decides which tile the chest will be placed upon
        Dim CN As Integer = 0

        For X = 0 To gridX
            For Y = 0 To gridY
                If mapStuff(X, Y).terrain = "grass" Then    'Only places chest on grass so the player can walk on it
                    If RN = CN Then
                        mapStuff(X, Y).terrain = "chest"
                        mapgrid(X, Y).BackColor = Color.Yellow
                        ChestX = X
                        ChestY = Y
                    End If
                    CN += 1
                End If
            Next
        Next
    End Sub

    Private Sub ItemDrop()
        Dim LootChance As Integer = RandomNumber(0, 10)
        Dim LootIndex As Integer
        Dim Loot As String
        Dim AttackIncrease As Integer
        If LootChance < 5 Then
            LootIndex = RandomNumber(0, 2)
            AttackIncrease = 1
        ElseIf LootChance < 8 Then
            LootIndex = RandomNumber(3, 4)
            AttackIncrease = 2
        ElseIf LootChance < 11 Then
            LootIndex = 5
            AttackIncrease = 3
        End If
        Loot = PlayerItems(LootIndex)
        Form3.playerPhys += AttackIncrease
        MsgBox("You have found " & Loot & "! Attack strength increased by " & AttackIncrease & ".")
    End Sub

    Private Sub MonsterMovement()
        Dim monsterTiles As New List(Of PictureBox)
        Dim NMT As New List(Of PictureBox)
        Dim OffensiveEnemy As New List(Of PictureBox)
        Dim MoveTowardsPlayer As Boolean = False

        For x = 0 To gridX
            For y = 0 To gridY
                If mapStuff(x, y).item = "monster" Then
                    If mapStuff(x, y).terrain = "grass" Then
                        If MoveTowardsPlayer = False Then
                            OffensiveEnemy.Add(mapgrid(x, y))
                        Else
                            monsterTiles.Add(mapgrid(x, y))
                        End If
                    End If
                End If
            Next
        Next

        NMT = NewMonsterTiles(monsterTiles, OffensiveEnemy)

        For x = 0 To gridX
            For y = 0 To gridY
                If NMT.Contains(mapgrid(x, y)) Then
                    mapStuff(x, y).item = "monster"
                    Intersection()
                End If
            Next
        Next

        LoadMonsters()
    End Sub

    Private Function NewMonsterTiles(monsterTiles As List(Of PictureBox), OffensiveEnemy As List(Of PictureBox))
        Dim NMT As New List(Of PictureBox)
        Dim NewX As Integer
        Dim NewY As Integer
        Dim RNDMovement As Integer
        For x = 0 To gridX
            For y = 0 To gridY
                If monsterTiles.Contains(mapgrid(x, y)) Then
                    RNDMovement = RandomNumber(0, 3)
                    If RNDMovement = 0 Then
                        NewX = x
                        NewY = y - 1
                    ElseIf RNDMovement = 1 Then
                        NewX = x + 1
                        NewY = y
                    ElseIf RNDMovement = 2 Then
                        NewX = x
                        NewY = y + 1
                    Else
                        NewX = x - 1
                        NewY = y
                    End If
                    Try
                        If mapStuff(NewX, NewY).terrain = "grass" Then
                            If mapStuff(NewX, NewY).item = "monster" Then
                                'Do Nothing
                            Else
                                mapStuff(x, y).item = "none"
                                mapgrid(x, y).Image = Nothing
                                NMT.Add(mapgrid(NewX, NewY))
                            End If
                        End If
                    Catch ex As Exception
                    End Try
                ElseIf OffensiveEnemy.Contains(mapgrid(x, y)) Then
                    If x > dudeX Then
                        NewX = x - 1
                    ElseIf x < dudeX Then
                        NewX = x + 1
                    End If
                    If y > dudeY Then
                        NewY = y - 1
                    ElseIf y < dudeY Then
                        NewY = y + 1
                    End If
                    Try
                        If mapStuff(NewX, NewY).terrain = "grass" Then
                            If mapStuff(NewX, NewY).item = "monster" Then
                                'Do Nothing
                            Else
                                mapStuff(x, y).item = "none"
                                mapgrid(x, y).Image = Nothing
                                NMT.Add(mapgrid(NewX, NewY))
                            End If
                        End If
                    Catch ex As Exception
                    End Try
                End If
            Next
        Next
        Return NMT
    End Function

    Private Function RandomNumber(N1 As Integer, N2 As Integer)
        Dim RND As New Random
        Dim D As Integer
        D = RND.Next(N1, N2)
        Return D
    End Function

End Class
